<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.redhat.training</groupId>
  <artifactId>bookstore</artifactId>
  <version>1.0</version>
  <name>The Shoppe Book Store</name>
  <description>The Java EE book store application</description>
  <packaging>war</packaging>
  <url>http://jboss.org/jbossas</url>
  <licenses>
    <license>
      <name>Apache License, Version 2.0</name>
      <distribution>repo</distribution>
      <url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
    </license>
  </licenses>
  <properties>
    <!-- Explicitly declaring the source encoding eliminates the following message: -->
    <!-- [WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent! -->
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <maven.compiler.target>1.8</maven.compiler.target>
    <maven.compiler.source>1.8</maven.compiler.source>

    <version.jboss.bom.eap>7.0.6.GA</version.jboss.bom.eap>
    <version.kie.bom>6.5.0.Final-redhat-9</version.kie.bom>

    <!-- This is to match warName of maven-war-plugin with wildfly:deploy. -->
    <wildfly.deployment.filename>bookstore.war</wildfly.deployment.filename>

    <!-- These are from JBoss EAP parent BOM. -->
    <version.mvn.wildfly>1.0.2.Final</version.mvn.wildfly>
    <version.mvn.surefire>2.17</version.mvn.surefire>
    <version.mvn.war>3.1.0</version.mvn.war>
    <version.arquillian.graphene.bom>2.1.0.CR2</version.arquillian.graphene.bom>
    <version.org.wildfly.arquillian.container>2.0.0.Final</version.org.wildfly.arquillian.container>

    <!-- RichFaces' last release only exists as individual components. -->
    <version.richfaces.comp>4.5.17.Final</version.richfaces.comp>

  </properties>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.jboss.bom</groupId>
        <artifactId>jboss-eap-javaee7-with-tools</artifactId>
        <version>${version.jboss.bom.eap}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>org.kie</groupId>
        <artifactId>kie-platform-bom</artifactId>
        <version>${version.kie.bom}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <dependencies>
    <!-- Application dependencies (provided by platform) -->
    <dependency>
      <groupId>org.jboss.spec.javax.faces</groupId>
      <artifactId>jboss-jsf-api_2.2_spec</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.hibernate.javax.persistence</groupId>
      <artifactId>hibernate-jpa-2.1-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.jboss.spec.javax.ejb</groupId>
      <artifactId>jboss-ejb-api_3.2_spec</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.jboss.spec.javax.ws.rs</groupId>
      <artifactId>jboss-jaxrs-api_2.0_spec</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>javax.enterprise</groupId>
      <artifactId>cdi-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.jboss.spec.javax.annotation</groupId>
      <artifactId>jboss-annotations-api_1.2_spec</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.jboss.spec.javax.servlet</groupId>
      <artifactId>jboss-servlet-api_3.1_spec</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-validator</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-search-orm</artifactId>
      <scope>provided</scope>
    </dependency>
    <!-- Application dependencies (bundled) -->
    <dependency>
      <groupId>org.richfaces</groupId>
      <artifactId>richfaces</artifactId>
      <version>4.5.17.Final</version>
    </dependency>
    <dependency>
      <groupId>org.richfaces</groupId>
      <artifactId>richfaces-a4j</artifactId>
      <version>4.5.17.Final</version>
    </dependency>
    <dependency>
      <groupId>org.drools</groupId>
      <artifactId>drools-compiler</artifactId>
    </dependency>
    <dependency>
      <groupId>org.kie</groupId>
      <artifactId>kie-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.kie</groupId>
      <artifactId>kie-ci</artifactId>
    </dependency>
    <dependency>
      <groupId>org.drools</groupId>
      <artifactId>drools-core</artifactId>
    </dependency>
    <!-- Testing dependencies -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.jboss.arquillian.junit</groupId>
      <artifactId>arquillian-junit-container</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.jboss.arquillian.graphene</groupId>
      <artifactId>graphene-webdriver</artifactId>
      <version>${version.arquillian.graphene.bom}</version>
      <type>pom</type>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.wildfly.arquillian</groupId>
      <artifactId>wildfly-arquillian-container-remote</artifactId>
      <version>${version.org.wildfly.arquillian.container}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.jboss.xnio</groupId>
      <artifactId>xnio-api</artifactId>
      <version>3.3.6.Final-redhat-1</version>
    </dependency>
  </dependencies>
  <build>
    <plugins>
      <plugin>
        <groupId>org.wildfly.plugins</groupId>
        <artifactId>wildfly-maven-plugin</artifactId>
	<version>${version.mvn.wildfly}</version>
      </plugin>
      <plugin>
        <artifactId>maven-war-plugin</artifactId>
	<version>${version.mvn.war}</version>
        <extensions>false</extensions>
        <configuration>
          <failOnMissingWebXml>false</failOnMissingWebXml>
	  <warName>bookstore</warName>
        </configuration>
      </plugin>
      <plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-surefire-plugin</artifactId>
	<version>${version.mvn.surefire}</version>
      </plugin>
    </plugins>
  </build>
  <profiles>
    <profile>
      <!-- When built in OpenShift the 'openshift' profile will be used when invoking mvn. -->
      <!-- Use this profile for any OpenShift specific customization your app will need. -->
      <!-- By default that is to put the resulting archive into the 'deployments' folder. -->
      <!-- http://maven.apache.org/guides/mini/guide-building-for-different-environments.html -->
      <id>openshift</id>
      <build>
        <finalName>bookstore</finalName>
        <plugins>
          <plugin>
	    <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-war-plugin</artifactId>
	    <version>${version.mvn.war}</version>
            <configuration>
              <outputDirectory>deployments</outputDirectory>
              <warName>ROOT</warName>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>

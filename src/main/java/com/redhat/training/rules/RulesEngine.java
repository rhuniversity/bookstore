package com.redhat.training.rules;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.redhat.training.domain.CatalogItem;
import com.redhat.training.domain.Promotion;
import com.redhat.training.view.ShoppingCart;


@Singleton
@Startup
public class RulesEngine  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private KieContainer container;

	public Collection<Object> fire(Object ... facts) {

		KieSession ksession = container.newKieSession("DiscountSession");

		if (ksession == null)
			throw new IllegalStateException("KIE DiscountSession was not built");
		
		for(Object fact : facts) {
			ksession.insert(fact);
		}
	
		ksession.fireAllRules();
		
		Collection<Object> updatedFacts = new ArrayList<Object>();
		for (Object o : ksession.getObjects())
			updatedFacts.add(o);
		
		ksession.dispose();
		
		return updatedFacts;
	}
	
	public BigDecimal fire(List<CatalogItem> catalogItems,Set<Promotion> promotions, ShoppingCart cart) {
		
		KieSession ksession = container.newKieSession("DiscountSession");
		if (ksession == null)
			throw new IllegalStateException("KIE DiscountSession was not built");
		
		for(Object item : catalogItems) {
			ksession.insert(item);
		}
		
		for (Promotion promotion : promotions) {
			ksession.insert(promotion);
		}
		ksession.insert(cart);
	
		ksession.fireAllRules();
		
		BigDecimal finalDiscountValue = BigDecimal.ZERO;
		for (Object o : ksession.getObjects())
			if (o instanceof BigDecimal)
			  finalDiscountValue = finalDiscountValue.add((BigDecimal) o);
		ksession.dispose();
		return finalDiscountValue;
	}
	
	@PostConstruct
	public void build(){
		KieServices kieServices = KieServices.Factory.get();
		container = kieServices.newKieClasspathContainer();
		if(RuleUtils.checkErrors(container)){
			throw new IllegalArgumentException("There are rule syntax errors");
		}

	}
	
}
